use intgt;

CREATE TABLE empleado (
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    codigo int,
    Apellidos VARCHAR(50),
    Nombres VARCHAR(50),
    DPI INT,
    NIT INT,
    TEL1 INT,
    Unidad VARCHAR(50),
    Depto VARCHAR(50),
    Puesto VARCHAR(50),
    fecha_de_ingreso VARCHAR(10)
);

CREATE TABLE Producto (
    sku VARCHAR(10),
    Marca VARCHAR(50),
    Modelo VARCHAR(50),
    No_Serie INT,
    Descripcion VARCHAR(50),
    No_Fac INT,
    Fech_Fac VARCHAR(50),
    Monto_Fac int
);

CREATE TABLE useradmin (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    usuario VARCHAR(50),
    pass VARCHAR(50),
    fecha_de_nacimiento VARCHAR(50),
    edad INT,
    foto LONGBLOB
);

CREATE TABLE usercp (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    usuario VARCHAR(50),
    pass VARCHAR(50),
    fecha_de_nacimiento VARCHAR(50),
    edad INT,
    foto LONGBLOB
);

CREATE TABLE usertel (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    usuario VARCHAR(50),
    pass VARCHAR(50),
    fecha_de_nacimiento VARCHAR(50),
    edad INT,
    foto LONGBLOB
);

CREATE TABLE userhhrr (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    usuario VARCHAR(50),
    pass VARCHAR(50),
    fecha_de_nacimiento VARCHAR(50),
    edad INT,
    foto LONGBLOB
);

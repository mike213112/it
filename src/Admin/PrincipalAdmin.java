/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin;

import Computo.*;
import Telefonia.*;
import Hhrr.*;
import java.awt.Color;
import Interfaz_de_Logins.PrincipalLogin;
import Base.Conexion;
import Employee.*;

/**
 *
 * @author mike
 */
public class PrincipalAdmin extends javax.swing.JFrame {

    public Conexion conn;
    public AgregarAdmin admin;
    public AgregarCP cp;
    public Agregartel tel;
    public Agregarhhrr hhrr;
    public Employee employee;
    public VerAdmin viewadmin;
    public String userhhrr = "0";
    public String usercp = "0";
    public String usertel = "0";
    public String useradmin = "0";
    public String viewemployee = "0";
    public String veradmin = "0";
    
    /**
     * Creates new form PrincipalAdmin
     */
    public PrincipalAdmin() {
        initComponents();
        setLocationRelativeTo(null);
        OcultarAddUser();
        OcultarViewUser();
        OcultarAddEmployee();
        OcultarViewEmployee();
    }

    public void OcultarAddUser(){
        btnhhrr.setVisible(false);
        btncp.setVisible(false);
        btntel.setVisible(false);
        btnadmin.setVisible(false);
        lblhhrr.setVisible(false);
        lblcp.setVisible(false);
        lbltel.setVisible(false);
        lbladmin.setVisible(false);
        Tab.setVisible(false);
   }
    
    public void VerAddUser(){
        btnhhrr.setVisible(true);
        btncp.setVisible(true);
        btntel.setVisible(true);
        btnadmin.setVisible(true);
        lblhhrr.setVisible(true);
        lblcp.setVisible(true);
        lbltel.setVisible(true);
        lbladmin.setVisible(true);
        btnprincipal.setBackground(new Color(23,35,51));
        btnadduser.setBackground(new Color(61,88,123));
        btnviewuser.setBackground(new Color(23,35,51));
        btnnewemployee.setBackground(new Color(23,35,51));
        btnviewemployee.setBackground(new Color(23,35,51));
        btnperfil.setBackground(new Color(23,35,51));
    }
    
    public void OcultarViewUser(){
        btnviewhhrr.setVisible(false);
        btnviewcp.setVisible(false);
        btnviewtel.setVisible(false);
        btnviewadmin.setVisible(false);
        lblviewhhrr.setVisible(false);
        lblviewcp.setVisible(false);
        lblviewtel.setVisible(false);
        lblviewadmin.setVisible(false);
        Tab2.setVisible(false);
    }
    
    public void VerViewUser(){
        btnviewhhrr.setVisible(true);
        btnviewcp.setVisible(true);
        btnviewtel.setVisible(true);
        btnviewadmin.setVisible(true);
        lblviewhhrr.setVisible(true);
        lblviewcp.setVisible(true);
        lblviewtel.setVisible(true);
        lblviewadmin.setVisible(true);
        btnprincipal.setBackground(new Color(23,35,51));
        btnadduser.setBackground(new Color(23,35,51));
        btnviewuser.setBackground(new Color(61,88,123));
        btnnewemployee.setBackground(new Color(23,35,51));
        btnviewemployee.setBackground(new Color(23,35,51));
        btnperfil.setBackground(new Color(23,35,51));
    }
    
    public void OcultarAddEmployee(){
        btnnewemployees.setVisible(false);
        lblnewemployee.setVisible(false);
        Tab3.setVisible(false);
    }
    
    public void MostrarAddEmployee(){
        btnnewemployees.setVisible(true);
        lblnewemployee.setVisible(true);
        btnprincipal.setBackground(new Color(23,35,51));
        btnadduser.setBackground(new Color(23,35,51));
        btnviewuser.setBackground(new Color(23,35,51));
        btnnewemployee.setBackground(new Color(61,88,123));
        btnviewemployee.setBackground(new Color(23,35,51));
        btnperfil.setBackground(new Color(23,35,51));
    }
    
    public void OcultarViewEmployee(){
        lblviewemployee.setVisible(false);
        btnveremployee.setVisible(false);
        Tab4.setVisible(false);
    }
    
    public void MostrarViewEmployee(){
        lblviewemployee.setVisible(true);
        btnveremployee.setVisible(true);
        btnprincipal.setBackground(new Color(23,35,51));
        btnadduser.setBackground(new Color(23,35,51));
        btnviewuser.setBackground(new Color(23,35,51));
        btnnewemployee.setBackground(new Color(23,35,51));
        btnviewemployee.setBackground(new Color(61,88,123));
        btnperfil.setBackground(new Color(23,35,51));
    }
    
    public void OcultarPerfil(){
        Tab5.setVisible(false);
    }
    
    public void MostrarPerfil(){
        btnprincipal.setBackground(new Color(23,35,51));
        btnadduser.setBackground(new Color(23,35,51));
        btnviewuser.setBackground(new Color(23,35,51));
        btnnewemployee.setBackground(new Color(23,35,51));
        btnviewemployee.setBackground(new Color(23,35,51));
        btnperfil.setBackground(new Color(61,88,123));
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnprincipal = new java.awt.Button();
        jLabel2 = new javax.swing.JLabel();
        btnadduser = new java.awt.Button();
        jLabel3 = new javax.swing.JLabel();
        btnviewuser = new java.awt.Button();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnnewemployee = new java.awt.Button();
        jLabel6 = new javax.swing.JLabel();
        btnviewemployee = new java.awt.Button();
        btnCerrar = new java.awt.Button();
        jLabel8 = new javax.swing.JLabel();
        btnSalir = new java.awt.Button();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        btnperfil = new java.awt.Button();
        jPanel5 = new javax.swing.JPanel();
        btnhhrr = new java.awt.Button();
        lblhhrr = new javax.swing.JLabel();
        btncp = new java.awt.Button();
        lblcp = new javax.swing.JLabel();
        lbltel = new javax.swing.JLabel();
        btntel = new java.awt.Button();
        lbladmin = new javax.swing.JLabel();
        btnadmin = new java.awt.Button();
        lblviewhhrr = new javax.swing.JLabel();
        btnviewhhrr = new java.awt.Button();
        lblviewcp = new javax.swing.JLabel();
        btnviewcp = new java.awt.Button();
        lblviewtel = new javax.swing.JLabel();
        btnviewtel = new java.awt.Button();
        lblviewadmin = new javax.swing.JLabel();
        btnviewadmin = new java.awt.Button();
        btnveremployee = new java.awt.Button();
        lblviewemployee = new javax.swing.JLabel();
        btnnewemployees = new java.awt.Button();
        lblnewemployee = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jSeparator2 = new javax.swing.JSeparator();
        Tab = new javax.swing.JTabbedPane();
        Tab2 = new javax.swing.JTabbedPane();
        Tab3 = new javax.swing.JTabbedPane();
        Tab4 = new javax.swing.JTabbedPane();
        Tab5 = new javax.swing.JTabbedPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(23, 35, 51));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(23, 35, 51));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("C059", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Dashboard");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        btnprincipal.setBackground(new java.awt.Color(23, 35, 51));
        btnprincipal.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnprincipal.setForeground(new java.awt.Color(255, 255, 255));
        btnprincipal.setLabel("Principal");
        btnprincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnprincipalActionPerformed(evt);
            }
        });
        jPanel2.add(btnprincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 180, 40));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hogar.png"))); // NOI18N
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 92, -1, -1));

        btnadduser.setBackground(new java.awt.Color(23, 35, 51));
        btnadduser.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnadduser.setForeground(new java.awt.Color(255, 255, 255));
        btnadduser.setLabel("Agregar Usuarios");
        btnadduser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnadduserActionPerformed(evt);
            }
        });
        jPanel2.add(btnadduser, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, 180, 40));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario.png"))); // NOI18N
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 142, -1, -1));

        btnviewuser.setBackground(new java.awt.Color(23, 35, 51));
        btnviewuser.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnviewuser.setForeground(new java.awt.Color(255, 255, 255));
        btnviewuser.setLabel("Ver Usuarios");
        btnviewuser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewuserActionPerformed(evt);
            }
        });
        jPanel2.add(btnviewuser, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, 180, 40));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 244, -1, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario.png"))); // NOI18N
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 194, -1, -1));

        btnnewemployee.setBackground(new java.awt.Color(23, 35, 51));
        btnnewemployee.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnnewemployee.setForeground(new java.awt.Color(255, 255, 255));
        btnnewemployee.setLabel("Agregar Empleado");
        btnnewemployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnewemployeeActionPerformed(evt);
            }
        });
        jPanel2.add(btnnewemployee, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, 180, 40));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 294, -1, -1));

        btnviewemployee.setBackground(new java.awt.Color(23, 35, 51));
        btnviewemployee.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnviewemployee.setForeground(new java.awt.Color(255, 255, 255));
        btnviewemployee.setLabel("Ver Empleado");
        btnviewemployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewemployeeActionPerformed(evt);
            }
        });
        jPanel2.add(btnviewemployee, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 290, 180, 40));

        btnCerrar.setBackground(new java.awt.Color(23, 35, 51));
        btnCerrar.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnCerrar.setForeground(new java.awt.Color(255, 255, 255));
        btnCerrar.setLabel("Cerrar Sesion");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        jPanel2.add(btnCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, 180, 40));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hogar.png"))); // NOI18N
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 394, -1, -1));

        btnSalir.setBackground(new java.awt.Color(23, 35, 51));
        btnSalir.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnSalir.setForeground(new java.awt.Color(255, 255, 255));
        btnSalir.setLabel("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jPanel2.add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 440, 180, 40));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hogar.png"))); // NOI18N
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 444, -1, -1));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/usuario-de-perfil.png"))); // NOI18N
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 344, -1, -1));

        btnperfil.setBackground(new java.awt.Color(23, 35, 51));
        btnperfil.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnperfil.setForeground(new java.awt.Color(255, 255, 255));
        btnperfil.setLabel("Perfil");
        btnperfil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnperfilActionPerformed(evt);
            }
        });
        jPanel2.add(btnperfil, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, 180, 40));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 490));

        jPanel5.setBackground(new java.awt.Color(84, 127, 206));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnhhrr.setBackground(new java.awt.Color(84, 127, 206));
        btnhhrr.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnhhrr.setForeground(new java.awt.Color(255, 255, 255));
        btnhhrr.setLabel("HHRR");
        btnhhrr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhhrrActionPerformed(evt);
            }
        });
        jPanel5.add(btnhhrr, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 180, 40));

        lblhhrr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario.png"))); // NOI18N
        jPanel5.add(lblhhrr, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 14, -1, -1));

        btncp.setBackground(new java.awt.Color(84, 127, 206));
        btncp.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btncp.setForeground(new java.awt.Color(255, 255, 255));
        btncp.setLabel("COMPUTO");
        btncp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncpActionPerformed(evt);
            }
        });
        jPanel5.add(btncp, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, 180, 40));

        lblcp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario.png"))); // NOI18N
        jPanel5.add(lblcp, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 14, -1, -1));

        lbltel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario.png"))); // NOI18N
        jPanel5.add(lbltel, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 14, -1, -1));

        btntel.setBackground(new java.awt.Color(84, 127, 206));
        btntel.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btntel.setForeground(new java.awt.Color(255, 255, 255));
        btntel.setLabel("TELEFONIA");
        btntel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntelActionPerformed(evt);
            }
        });
        jPanel5.add(btntel, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 10, 180, 40));

        lbladmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario.png"))); // NOI18N
        jPanel5.add(lbladmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 14, -1, -1));

        btnadmin.setBackground(new java.awt.Color(84, 127, 206));
        btnadmin.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnadmin.setForeground(new java.awt.Color(255, 255, 255));
        btnadmin.setLabel("ADMIN");
        btnadmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnadminActionPerformed(evt);
            }
        });
        jPanel5.add(btnadmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 10, 180, 40));

        lblviewhhrr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jPanel5.add(lblviewhhrr, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 14, -1, -1));

        btnviewhhrr.setBackground(new java.awt.Color(84, 127, 206));
        btnviewhhrr.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnviewhhrr.setForeground(new java.awt.Color(255, 255, 255));
        btnviewhhrr.setLabel("VER HHRR");
        btnviewhhrr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewhhrrActionPerformed(evt);
            }
        });
        jPanel5.add(btnviewhhrr, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 180, 40));

        lblviewcp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jPanel5.add(lblviewcp, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 14, -1, -1));

        btnviewcp.setBackground(new java.awt.Color(84, 127, 206));
        btnviewcp.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnviewcp.setForeground(new java.awt.Color(255, 255, 255));
        btnviewcp.setLabel("VER COMPUTO");
        btnviewcp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewcpActionPerformed(evt);
            }
        });
        jPanel5.add(btnviewcp, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, 180, 40));

        lblviewtel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jPanel5.add(lblviewtel, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 14, -1, -1));

        btnviewtel.setBackground(new java.awt.Color(84, 127, 206));
        btnviewtel.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnviewtel.setForeground(new java.awt.Color(255, 255, 255));
        btnviewtel.setLabel("VER TELEFONIA");
        btnviewtel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewtelActionPerformed(evt);
            }
        });
        jPanel5.add(btnviewtel, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 10, 180, 40));

        lblviewadmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jPanel5.add(lblviewadmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 14, -1, -1));

        btnviewadmin.setBackground(new java.awt.Color(84, 127, 206));
        btnviewadmin.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnviewadmin.setForeground(new java.awt.Color(255, 255, 255));
        btnviewadmin.setLabel("VER ADMIN");
        btnviewadmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewadminActionPerformed(evt);
            }
        });
        jPanel5.add(btnviewadmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 10, 180, 40));

        btnveremployee.setBackground(new java.awt.Color(84, 127, 206));
        btnveremployee.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnveremployee.setForeground(new java.awt.Color(255, 255, 255));
        btnveremployee.setLabel("VER EMPLEADO");
        btnveremployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnveremployeeActionPerformed(evt);
            }
        });
        jPanel5.add(btnveremployee, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 10, 180, 40));

        lblviewemployee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jPanel5.add(lblviewemployee, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 14, -1, -1));

        btnnewemployees.setBackground(new java.awt.Color(84, 127, 206));
        btnnewemployees.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnnewemployees.setForeground(new java.awt.Color(255, 255, 255));
        btnnewemployees.setLabel("EMPLEADO");
        btnnewemployees.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnewemployeesActionPerformed(evt);
            }
        });
        jPanel5.add(btnnewemployees, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 10, 180, 40));

        lblnewemployee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario.png"))); // NOI18N
        jPanel5.add(lblnewemployee, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 14, -1, -1));

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 0, 1050, 60));

        jPanel3.setBackground(new java.awt.Color(71, 120, 197));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jSeparator2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1050, 60));
        jPanel3.add(Tab, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1050, 430));
        jPanel3.add(Tab2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1050, 430));
        jPanel3.add(Tab3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1050, 430));
        jPanel3.add(Tab4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1050, 430));
        jPanel3.add(Tab5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1050, 430));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 60, 1050, 430));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1287, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void AbrirHHRR(){
        hhrr = new Agregarhhrr();
        if(userhhrr.equals("0")){
            Tab.setVisible(true);
            Tab.addTab("Agregar Usuario HHRR", hhrr);
            Tab.setSelectedComponent(hhrr);
        }
        userhhrr = "1";
    }
    
    private void btnhhrrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhhrrActionPerformed
        // TODO add your handling code here:
        AbrirHHRR();
    }//GEN-LAST:event_btnhhrrActionPerformed

    public void AbrirADMIN(){
        admin = new AgregarAdmin();
        if(useradmin.equals("0")){
            Tab.setVisible(true);
            Tab.addTab("Agregar Usuario ADMIN", admin);
            Tab.setSelectedComponent(admin);
        }
        useradmin = "1";
    }
    
    private void btnadminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnadminActionPerformed
        // TODO add your handling code here:
        AbrirADMIN();
    }//GEN-LAST:event_btnadminActionPerformed

    public void AbrirCP(){
        cp = new AgregarCP();
        if(usercp.equals("0")){
            Tab.setVisible(true);
            Tab.addTab("Agregar Usuario Computo", cp);
            Tab.setSelectedComponent(cp);
        }
        usercp = "1";
    }
    
    private void btncpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncpActionPerformed
        // TODO add your handling code here:
        AbrirCP();
    }//GEN-LAST:event_btncpActionPerformed

    public void AbrirTEl(){
        tel = new Agregartel();
        if(usertel.equals("0")){
            Tab.setVisible(true);
            Tab.addTab("Agregar Usuario Telefonia", tel);
            Tab.setSelectedComponent(tel);
        }
        usertel = "1";
    }
    
    private void btntelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntelActionPerformed
        // TODO add your handling code here:
        AbrirTEl();
    }//GEN-LAST:event_btntelActionPerformed

    private void btnprincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnprincipalActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_btnprincipalActionPerformed

    private void btnviewhhrrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewhhrrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnviewhhrrActionPerformed

    private void btnviewcpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewcpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnviewcpActionPerformed

    private void btnviewtelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewtelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnviewtelActionPerformed

    public void VerUsuarioAdmin(){
        viewadmin = new VerAdmin();
        if(veradmin.equals("0")){
            Tab3.setVisible(true);
            Tab3.addTab("Ver Usuario Admin", viewadmin);
            Tab3.setSelectedComponent(viewadmin);
        }
        veradmin = "1";
    }
    
    private void btnviewadminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewadminActionPerformed
        // TODO add your handling code here:
        VerUsuarioAdmin();
    }//GEN-LAST:event_btnviewadminActionPerformed

    private void btnadduserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnadduserActionPerformed
        // TODO add your handling code here:
        VerAddUser();
        OcultarViewUser();
        OcultarPerfil();
        OcultarAddEmployee();
        OcultarViewEmployee();
    }//GEN-LAST:event_btnadduserActionPerformed

    private void btnviewuserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewuserActionPerformed
        // TODO add your handling code here:
        VerViewUser();
        OcultarAddUser();
        OcultarPerfil();
        OcultarAddEmployee();
        OcultarViewEmployee();
    }//GEN-LAST:event_btnviewuserActionPerformed

    public void AbrirEmpleado(){
        employee = new Employee();
        if(viewemployee.equals("0")){
            Tab4.setVisible(true);
            Tab4.addTab("Ver Empleado", employee);
            Tab4.setSelectedComponent(employee);
        }
        viewemployee = "1";
    }
    
    private void btnveremployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnveremployeeActionPerformed
        // TODO add your handling code here:
        AbrirEmpleado();
    }//GEN-LAST:event_btnveremployeeActionPerformed

    private void btnnewemployeesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnewemployeesActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_btnnewemployeesActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        PrincipalLogin login = new PrincipalLogin();
        login.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnviewemployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewemployeeActionPerformed
        // TODO add your handling code here:
        MostrarViewEmployee();
        OcultarAddEmployee();
        OcultarAddUser();
        OcultarPerfil();
        OcultarViewUser();
    }//GEN-LAST:event_btnviewemployeeActionPerformed

    private void btnnewemployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnewemployeeActionPerformed
        // TODO add your handling code here:
        MostrarAddEmployee();
        OcultarAddUser();
        OcultarPerfil();
        OcultarViewUser();
        OcultarViewEmployee();
    }//GEN-LAST:event_btnnewemployeeActionPerformed

    private void btnperfilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnperfilActionPerformed
        // TODO add your handling code here:
        OcultarAddEmployee();
        OcultarAddUser();
        OcultarViewUser();
        OcultarViewEmployee();
    }//GEN-LAST:event_btnperfilActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PrincipalAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PrincipalAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PrincipalAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PrincipalAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrincipalAdmin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane Tab;
    private javax.swing.JTabbedPane Tab2;
    private javax.swing.JTabbedPane Tab3;
    private javax.swing.JTabbedPane Tab4;
    private javax.swing.JTabbedPane Tab5;
    private java.awt.Button btnCerrar;
    private java.awt.Button btnSalir;
    private java.awt.Button btnadduser;
    private java.awt.Button btnadmin;
    private java.awt.Button btncp;
    private java.awt.Button btnhhrr;
    private java.awt.Button btnnewemployee;
    private java.awt.Button btnnewemployees;
    private java.awt.Button btnperfil;
    private java.awt.Button btnprincipal;
    private java.awt.Button btntel;
    private java.awt.Button btnveremployee;
    private java.awt.Button btnviewadmin;
    private java.awt.Button btnviewcp;
    private java.awt.Button btnviewemployee;
    private java.awt.Button btnviewhhrr;
    private java.awt.Button btnviewtel;
    private java.awt.Button btnviewuser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lbladmin;
    private javax.swing.JLabel lblcp;
    private javax.swing.JLabel lblhhrr;
    private javax.swing.JLabel lblnewemployee;
    private javax.swing.JLabel lbltel;
    private javax.swing.JLabel lblviewadmin;
    private javax.swing.JLabel lblviewcp;
    private javax.swing.JLabel lblviewemployee;
    private javax.swing.JLabel lblviewhhrr;
    private javax.swing.JLabel lblviewtel;
    // End of variables declaration//GEN-END:variables
}

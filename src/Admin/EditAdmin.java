/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin;

import java.awt.*;
import java.io.File;
import java.util.Calendar;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import Base.Conexion;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.imageio.ImageIO;
import Base.LeerUserAdmin;

/**
 *
 * @author mike
 */
public class EditAdmin extends javax.swing.JFrame {

    Calendar fecha = Calendar.getInstance();
    Conexion conn = new Conexion();
    File fichero;

    /**
     * Creates new form EditAdmin
     */
    public EditAdmin() {
        initComponents();
        setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtruta = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnameedit = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        txtlast_nameedit = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        txtuseredit = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        txtdateedit = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        txtedadedit = new javax.swing.JTextField();
        txtpassedit = new javax.swing.JPasswordField();
        fotoedit = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtid = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        btnsave = new java.awt.Button();
        jLabel10 = new javax.swing.JLabel();
        btnaddimages = new java.awt.Button();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(67, 107, 160));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("C059", 1, 22)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nombre:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        txtnameedit.setBackground(new java.awt.Color(67, 107, 160));
        txtnameedit.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        txtnameedit.setForeground(new java.awt.Color(255, 255, 255));
        txtnameedit.setBorder(null);
        jPanel1.add(txtnameedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 60, 178, 29));

        jSeparator1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 90, 180, 30));

        jLabel2.setFont(new java.awt.Font("C059", 1, 22)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Apellido:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, -1));

        txtlast_nameedit.setBackground(new java.awt.Color(67, 107, 160));
        txtlast_nameedit.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        txtlast_nameedit.setForeground(new java.awt.Color(255, 255, 255));
        txtlast_nameedit.setBorder(null);
        jPanel1.add(txtlast_nameedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 120, 178, 29));

        jSeparator2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 150, 180, 30));

        txtuseredit.setBackground(new java.awt.Color(67, 107, 160));
        txtuseredit.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        txtuseredit.setForeground(new java.awt.Color(255, 255, 255));
        txtuseredit.setBorder(null);
        jPanel1.add(txtuseredit, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 180, 178, 29));

        jSeparator3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 210, 180, 30));

        jLabel3.setFont(new java.awt.Font("C059", 1, 22)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Usuario:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, -1));

        jLabel4.setFont(new java.awt.Font("C059", 1, 22)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Contraseña:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, -1, -1));

        jSeparator4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 280, 180, 30));

        jLabel5.setFont(new java.awt.Font("C059", 1, 22)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Fecha de nacimiento:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, -1, -1));

        jSeparator5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 350, 180, 30));

        txtdateedit.setBackground(new java.awt.Color(67, 107, 160));
        txtdateedit.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        txtdateedit.setForeground(new java.awt.Color(255, 255, 255));
        txtdateedit.setBorder(null);
        txtdateedit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtdateeditFocusGained(evt);
            }
        });
        txtdateedit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdateeditKeyTyped(evt);
            }
        });
        jPanel1.add(txtdateedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 320, 178, 29));

        jLabel6.setFont(new java.awt.Font("C059", 1, 22)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Edad:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, -1, -1));

        jSeparator6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 420, 180, 30));

        txtedadedit.setEditable(false);
        txtedadedit.setBackground(new java.awt.Color(67, 107, 160));
        txtedadedit.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        txtedadedit.setForeground(new java.awt.Color(255, 255, 255));
        txtedadedit.setBorder(null);
        jPanel1.add(txtedadedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 390, 178, 29));

        txtpassedit.setBackground(new java.awt.Color(67, 107, 160));
        txtpassedit.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        txtpassedit.setForeground(new java.awt.Color(255, 255, 255));
        txtpassedit.setBorder(null);
        jPanel1.add(txtpassedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 250, 178, 29));

        fotoedit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/pordefecto.png"))); // NOI18N
        jPanel1.add(fotoedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 10, 280, 300));

        jLabel7.setFont(new java.awt.Font("C059", 1, 22)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Id:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        txtid.setEditable(false);
        txtid.setBackground(new java.awt.Color(67, 107, 160));
        txtid.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        txtid.setForeground(new java.awt.Color(255, 255, 255));
        txtid.setBorder(null);
        jPanel1.add(txtid, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 10, 100, 29));

        jSeparator7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 40, 100, 30));

        jPanel2.setBackground(new java.awt.Color(40, 84, 130));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/disquete (1).png"))); // NOI18N
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, -1));

        btnsave.setBackground(new java.awt.Color(67, 107, 160));
        btnsave.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnsave.setForeground(new java.awt.Color(255, 255, 255));
        btnsave.setLabel("Guardar Edicion");
        btnsave.setMaximumSize(new java.awt.Dimension(0, 0));
        btnsave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsaveActionPerformed(evt);
            }
        });
        jPanel2.add(btnsave, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, -1, -1));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/buscar-imagen.png"))); // NOI18N
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        btnaddimages.setBackground(new java.awt.Color(67, 107, 160));
        btnaddimages.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        btnaddimages.setForeground(new java.awt.Color(255, 255, 255));
        btnaddimages.setLabel("Cambiar Imagen");
        btnaddimages.setMaximumSize(new java.awt.Dimension(0, 0));
        btnaddimages.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaddimagesActionPerformed(evt);
            }
        });
        jPanel2.add(btnaddimages, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, -1, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 0, 240, 460));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1004, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 458, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtdateeditFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtdateeditFocusGained
        // TODO add your handling code here:
        txtdateedit.selectAll();
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        int mes = fecha.get(Calendar.MONTH);
        int año = fecha.get(Calendar.YEAR);
        int dd = 0, mm = 0, yy = 0;

        while (dd < 1 || dd > 31) {
            dd = Integer.parseInt(JOptionPane.showInputDialog("Ingrese dia"));
            if (dd < 1 || dd > 31) {
                JOptionPane.showMessageDialog(null, "Solo se permite del 1 al 31", "Dia incorrecto", 0);
            }
        }

        while (mm < 1 || mm > 12) {
            mm = Integer.parseInt(JOptionPane.showInputDialog("Ingrese mes"));
            if (mm < 1 || mm > 12) {
                JOptionPane.showMessageDialog(null, "Solo se permite del 1 al 12", "Mes incorrecto", 0);
            }
        }

        while (yy < 1900 || yy > año) {
            yy = Integer.parseInt(JOptionPane.showInputDialog("Ingrese año"));
            if (yy < 1800 || yy > año) {
                JOptionPane.showMessageDialog(null, "Solo se permite del 1900 al " + año, "Año incorrecto", 0);
            }
        }

        txtdateedit.setText(dd + "/" + mm + "/" + yy);

        txtedadedit.setText(String.valueOf(año - yy));
        txtedadedit.setEditable(false);
        btnaddimages.requestFocus();
        btnaddimages.setEnabled(true);
        btnaddimages.setForeground(Color.BLACK.WHITE);
        btnaddimages.setBackground(new Color(67, 107, 160));
        btnsave.setEnabled(true);
        btnsave.setForeground(Color.WHITE);
        btnsave.setBackground(new Color(67, 107, 160));
    }//GEN-LAST:event_txtdateeditFocusGained

    private void txtdateeditKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdateeditKeyTyped
        // TODO add your handling code here:
        char validdar = evt.getKeyChar();

        if (Character.isLetter(validdar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(null, "SOLO SE ACPETAN NUMEROS");
        }
    }//GEN-LAST:event_txtdateeditKeyTyped

    private void btnsaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsaveActionPerformed
        // TODO add your handling code here:

        String nombre = txtnameedit.getText();
        String apellido = txtlast_nameedit.getText();
        String usuario = txtuseredit.getText();
        String pass = String.valueOf(txtpassedit.getPassword());
        String fecha = txtdateedit.getText();
        int edad = Integer.parseInt(txtedadedit.getText());
        File ruta = new File(txtruta.getText());

        PreparedStatement into = null;
        PreparedStatement into2 = null;
        Connection conectar = conn.getConexion();
        String query = "UPDATE useradmin SET nombre=?,apellido=?,usuario=?,pass=?,fecha_de_nacimiento=?,edad=?,foto=? WHERE id=?";
        try {
            into = conectar.prepareStatement(query);
            into.setString(1, nombre);
            into.setString(2, apellido);
            into.setString(3, usuario);
            into.setString(4, pass);
            into.setString(5, fecha);
            into.setInt(6, edad);
            try {
                byte[] icono = new byte[(int) ruta.length()];
                InputStream input = new FileInputStream(ruta);
                input.read(icono);
                into.setBytes(7, icono);
                into.setString(8, txtid.getText());
                into.execute();
                JOptionPane.showMessageDialog(null, "Los datos se editaron correctamente");
                btnsave.setEnabled(false);
                btnsave.setBackground(Color.DARK_GRAY);
                btnaddimages.setEnabled(false);
                btnaddimages.setBackground(Color.DARK_GRAY);
            } catch (Exception ex) {
                LeerUserAdmin leer = new LeerUserAdmin();
                int id = Integer.parseInt(txtid.getText());
                String SQL = "SELECT foto FROM useradmin WHERE id='" + id + "'";
                ResultSet resutl = leer.leer(SQL);
                while (resutl.next()) {
                    try {
                        byte[] bi = resutl.getBytes(1);
                        into.setBytes(7, bi);
                        into.setString(8, txtid.getText());
                        into.execute();
                        JOptionPane.showMessageDialog(null, "Los Datos no se pudieron editar");
                        btnsave.setEnabled(false);
                        btnsave.setBackground(Color.DARK_GRAY);
                        btnaddimages.setEnabled(false);
                        btnaddimages.setBackground(Color.DARK_GRAY);
                        VerAdmin ver = new VerAdmin();
                        ver.setVisible(true);
                        dispose();
                    } catch (Exception ex1) {
                        JOptionPane.showMessageDialog(null, ex1);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error " + e);
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                conectar.close();
            } catch (Exception e) {
                System.out.println("Error " + e);
                JOptionPane.showMessageDialog(null, e);
            }
        }

    }//GEN-LAST:event_btnsaveActionPerformed

    private void btnaddimagesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaddimagesActionPerformed
        int resultado;
        JFileChooser j = new JFileChooser();
        FileNameExtensionFilter fil = new FileNameExtensionFilter("JPG, JPEG, PNG, BMP, GIF", "jpg", "jpeg", "png", "bmp", "gif");
        j.setFileFilter(fil);

        int s = j.showOpenDialog(this);
        if (s == JFileChooser.APPROVE_OPTION) {
            String ruta = j.getSelectedFile().getAbsolutePath();
            txtruta.setText(ruta);
            fichero = j.getSelectedFile();
            try {
                ImageIcon icon = new ImageIcon(fichero.toString());

                Icon icono = new ImageIcon(icon.getImage().getScaledInstance(fotoedit.getWidth(), fotoedit.getHeight(), Image.SCALE_DEFAULT));

                fotoedit.setText(null);
                fotoedit.setIcon(icono);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error al abrir imagen " + e);
            }
        }
    }//GEN-LAST:event_btnaddimagesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditAdmin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Button btnaddimages;
    private java.awt.Button btnsave;
    public javax.swing.JLabel fotoedit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    public javax.swing.JTextField txtdateedit;
    public javax.swing.JTextField txtedadedit;
    public javax.swing.JTextField txtid;
    public javax.swing.JTextField txtlast_nameedit;
    public javax.swing.JTextField txtnameedit;
    public javax.swing.JPasswordField txtpassedit;
    private javax.swing.JTextField txtruta;
    public javax.swing.JTextField txtuseredit;
    // End of variables declaration//GEN-END:variables
}

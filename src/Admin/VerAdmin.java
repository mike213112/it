/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin;

import Base.LeerUserAdmin;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.sql.ResultSet;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import javax.swing.JButton;
import javax.swing.RowFilter;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import Base.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mike
 */
public class VerAdmin extends javax.swing.JPanel {

    LeerUserAdmin leer = new LeerUserAdmin();
    JLabel fotoimagen;
    JLabel fotoimagenedit;
    private static int row, columna;
    JButton editar = new JButton("Editar");
    JButton eliminar = new JButton("Eliminar");
    TableColumnModel columnModel;
    TableRowSorter trs;
    Conexion conn = new Conexion();
    EditAdmin editadmin = new EditAdmin();
    PrincipalAdmin viewadmin = new PrincipalAdmin();

    /**
     * Creates new form Employee
     */
    public VerAdmin() {
        initComponents();
        LeerDatos();
        cbxBuscar.setEditable(false);
        tableAdmin.getTableHeader().setOpaque(false);
        tableAdmin.getTableHeader().setBackground(new Color(32, 136, 203));
        tableAdmin.getTableHeader().setForeground(new Color(255, 255, 255));
        editar.setBackground(new Color(107, 152, 211));
        editar.setForeground(new Color(255, 255, 255));
        editar.setIcon(new ImageIcon(getClass().getResource("/Imagenes/paginas.png")));
        eliminar.setBackground(new Color(107, 152, 211));
        eliminar.setForeground(new Color(255, 255, 255));
        eliminar.setIcon(new ImageIcon(getClass().getResource("/Imagenes/eliminar.png")));
        columnModel = tableAdmin.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(30);
        columnModel.getColumn(1).setPreferredWidth(150);
        columnModel.getColumn(2).setPreferredWidth(150);
        columnModel.getColumn(3).setPreferredWidth(95);
        columnModel.getColumn(4).setPreferredWidth(95);
        columnModel.getColumn(5).setPreferredWidth(90);
        columnModel.getColumn(6).setPreferredWidth(50);
        columnModel.getColumn(7).setPreferredWidth(60);
        columnModel.getColumn(8).setPreferredWidth(130);
        columnModel.getColumn(9).setPreferredWidth(140);
    }

    public void LeerDatos() {
        DefaultTableModel model = new DefaultTableModel();

        //tableAdmin.setEnabled(false);
        model.addColumn("Id");
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Usuario");
        model.addColumn("Contraseña");
        model.addColumn("Fecha");
        model.addColumn("Edad");
        model.addColumn("Foto");
        model.addColumn("Editar");
        model.addColumn("Eliminar");

        tableAdmin.setModel(model);

        Object[] datos = new Object[10];

        try {
            ResultSet resutl = leer.leer("select * from useradmin");
            tableAdmin.setDefaultRenderer(Object.class, new Render());
            while (resutl.next()) {
                datos[0] = resutl.getString(1);
                datos[1] = resutl.getString(2);
                datos[2] = resutl.getString(3);
                datos[3] = resutl.getString(4);
                datos[4] = resutl.getString(5);
                datos[5] = resutl.getString(6);
                int dato7 = Integer.parseInt("7");
                datos[6] = resutl.getInt(dato7);
                Object o;
                try {
                    byte[] bi = resutl.getBytes(8);
                    BufferedImage image = null;
                    image = ImageIO.read(new ByteArrayInputStream(bi));
                    ImageIcon imgi = new ImageIcon(image.getScaledInstance(60, 60, 5));
                    fotoimagen = new JLabel(imgi);
                    datos[7] = fotoimagen;
                    ImageIcon imgiedit = new ImageIcon(image);
                    fotoimagenedit = new JLabel(imgiedit);
                    conn.Desconectar();
                } catch (Exception ex) {
                    datos[7] = new JLabel("No imagen");
                    conn.Desconectar();
                }
                datos[8] = editar;
                datos[9] = eliminar;
                tableAdmin.setRowHeight(50);
                model.addRow(datos);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    public void Busqueda() {
        int columnaAbuscar = 0;

        if (cbxBuscar.getSelectedItem().equals("Seleccionar")) {

        }
        if (cbxBuscar.getSelectedItem().equals("Id")) {
            columnaAbuscar = 0;
        }
        if (cbxBuscar.getSelectedItem().equals("Nombre")) {
            columnaAbuscar = 1;
        }
        if (cbxBuscar.getSelectedItem().equals("Apellido")) {
            columnaAbuscar = 2;
        }
        if (cbxBuscar.getSelectedItem().equals("Usuario")) {
            columnaAbuscar = 3;
        }
        if (cbxBuscar.getSelectedItem().equals("Contraseña")) {
            columnaAbuscar = 4;
        }
        if (cbxBuscar.getSelectedItem().equals("Fecha")) {
            columnaAbuscar = 5;
        }
        if (cbxBuscar.getSelectedItem().equals("Edad")) {
            columnaAbuscar = 6;
        }
        trs.setRowFilter(RowFilter.regexFilter(txtBuscar.getText(), columnaAbuscar));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtid = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cbxBuscar = new javax.swing.JComboBox<>();
        txtBuscar = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableAdmin = new javax.swing.JTable();

        setBackground(new java.awt.Color(23, 35, 51));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(84, 127, 206));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/menu.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 20, -1, -1));

        cbxBuscar.setBackground(new java.awt.Color(84, 127, 206));
        cbxBuscar.setEditable(true);
        cbxBuscar.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        cbxBuscar.setForeground(new java.awt.Color(255, 255, 255));
        cbxBuscar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Id", "Nombre", "Apellido", "Usuario", "Contraseña", "Fecha", "Edad" }));
        jPanel1.add(cbxBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 20, 140, 35));

        txtBuscar.setBackground(new java.awt.Color(84, 127, 206));
        txtBuscar.setFont(new java.awt.Font("C059", 1, 18)); // NOI18N
        txtBuscar.setForeground(new java.awt.Color(255, 255, 255));
        txtBuscar.setBorder(null);
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBuscarKeyTyped(evt);
            }
        });
        jPanel1.add(txtBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 20, 180, 35));

        jSeparator1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 54, 180, 20));

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1044, 70));

        tableAdmin = new javax.swing.JTable(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        tableAdmin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tableAdmin.setSelectionBackground(new java.awt.Color(84, 127, 206));
        tableAdmin.setSelectionForeground(new java.awt.Color(255, 255, 255));
        tableAdmin.getTableHeader().setReorderingAllowed(false);
        tableAdmin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableAdminMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableAdmin);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 1044, 320));
    }// </editor-fold>//GEN-END:initComponents

    public void IraEditar() throws SQLException {
        DefaultTableModel modelo = (DefaultTableModel) tableAdmin.getModel();

        int seleccionarfilas = tableAdmin.getSelectedRow();
        if (seleccionarfilas >= 0) {
            editadmin.txtid.setText(tableAdmin.getValueAt(seleccionarfilas, 0).toString());
            txtid.setText(tableAdmin.getValueAt(seleccionarfilas, 0).toString());
            editadmin.txtnameedit.setText(tableAdmin.getValueAt(seleccionarfilas, 1).toString());
            editadmin.txtlast_nameedit.setText(tableAdmin.getValueAt(seleccionarfilas, 2).toString());
            editadmin.txtuseredit.setText(tableAdmin.getValueAt(seleccionarfilas, 3).toString());
            editadmin.txtpassedit.setText(tableAdmin.getValueAt(seleccionarfilas, 4).toString());
            editadmin.txtdateedit.setText(tableAdmin.getValueAt(seleccionarfilas, 5).toString());
            editadmin.txtedadedit.setText(tableAdmin.getValueAt(seleccionarfilas, 6).toString());
            int id = Integer.parseInt(txtid.getText());
            String SQL = "SELECT foto FROM useradmin WHERE id='" + id + "'";
            ResultSet resutl = leer.leer(SQL);

            while (resutl.next()) {
                try {
                    byte[] bi = resutl.getBytes(1);
                    BufferedImage image = null;
                    image = ImageIO.read(new ByteArrayInputStream(bi));
                    ImageIcon imgi = new ImageIcon(image.getScaledInstance(editadmin.fotoedit.getWidth(), editadmin.fotoedit.getHeight(), Image.SCALE_DEFAULT));
                    editadmin.fotoedit.setIcon(imgi);
                    viewadmin.setVisible(false);
                    viewadmin.dispose();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
            modelo.removeRow(seleccionarfilas);
            editadmin.setVisible(true);
            viewadmin.setVisible(false);
            viewadmin.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "No ha seleccionado ninguna fila");
        }
    }

    public void Eliminar() {

        DefaultTableModel modelo = (DefaultTableModel) tableAdmin.getModel();

        PreparedStatement into = null;
        Connection conectar = conn.getConexion();

        String query = "DELETE FROM useradmin WHERE id=?";

        int seleccionarfilas = tableAdmin.getSelectedRow();
        if (seleccionarfilas >= 0) {
            txtid.setText(tableAdmin.getValueAt(seleccionarfilas, 0).toString());
            int id = Integer.parseInt(txtid.getText());
            try {
                into = conectar.prepareStatement(query);
                into.setInt(1, id);
                into.execute();
                modelo.removeRow(seleccionarfilas);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            } finally {
                try {
                    conectar.close();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "No ha seleccionado ninguna fila");
        }
    }

    private void tableAdminMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableAdminMouseClicked
        // TODO add your handling code here:
        editar.setName("btneditar");
        eliminar.setName("btneliminar");
        columna = tableAdmin.getColumnModel().getColumnIndexAtX(evt.getX());
        row = evt.getY() / tableAdmin.getRowHeight();
        if (columna <= tableAdmin.getColumnCount() && columna >= 0 && row <= tableAdmin.getRowCount() && row >= 0) {
            Object objeto = tableAdmin.getValueAt(row, columna);
            if (objeto instanceof JButton) {
                ((JButton) objeto).doClick();
                JButton botones = (JButton) objeto;
                if (botones.getName().equals("btneditar")) {
                    try {
                        IraEditar();
                    } catch (SQLException ex) {
                        Logger.getLogger(VerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (botones.getName().equals("btneliminar")) {
                    Eliminar();
                }
            }
        }
    }//GEN-LAST:event_tableAdminMouseClicked

    private void txtBuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyTyped
        // TODO add your handling code here:
        txtBuscar.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (txtBuscar.getText());
                txtBuscar.setText(cadena);
                repaint();
                Busqueda();
            }
        });
        trs = new TableRowSorter(tableAdmin.getModel());
        tableAdmin.setRowSorter(trs);
    }//GEN-LAST:event_txtBuscarKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbxBuscar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tableAdmin;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtid;
    // End of variables declaration//GEN-END:variables
}

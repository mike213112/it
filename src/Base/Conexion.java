/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Base;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author miker
 */
public class Conexion {
    
    private static Connection conn = null;
    private static String driver = "com.mysql.jdbc.Driver";
    private static String base = "intgt";
    private static String user = "root";
    private static String pass = "root";
    private static String url = "jdbc:mysql://localhost:3309/" + base+ "?useSSL=false";    
    
    public Conexion(){
       try {
            Class.forName(driver);
            conn =  DriverManager.getConnection(url, user, pass);
            if (conn != null) {
                System.out.println("Conexion Satisfactoria");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Error al conectar con la base " + ex);
        }
    }
    
    public Connection getConexion(){
        return conn;
    }
    
    public void Desconectar(){
        if(conn == null){
            System.out.println("Conexion finalizada.........");
        }
    }
    
}

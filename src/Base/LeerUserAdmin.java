/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Base;

import java.sql.*;

/**
 *
 * @author mike
 */
public class LeerUserAdmin extends Conexion {
 
     public ResultSet leer(String leer){

        Connection conectar = getConexion();
        Statement stam;
        ResultSet datos = null;
        try {
            stam = conectar.createStatement();
            datos = stam.executeQuery(leer);
            conectar.close();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
        return datos;
    }
    
}
